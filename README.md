## Docker cheat sheet


### Docker status info
```
$ service docker start
$ service docker status
$ service docker stop
$ docker ps
$ docker info
```

### Docker image 
##### Pull image from docker registry
```
$ docker image pull
```
##### Show a list of all images on your system
```
$ docker image ls
```
##### Image and container directory
```
/var/lib/docker
```
##### Create an image from a container
```
$ docker container run -it <containerName> bash
$ docker container commit <containerName>
```
##### Load an image from the system to Docker

```
$ docker save -o <tarFile> <image>
$ docker load -i <tarFile>

```
##### Add a tag to the image
```
$ docker tag <imageName> <repoName>:<tagName>
```
### Docker container
##### Show container list
```
$ docker container ls
$ docker container ls-a
```
##### Create/remove a container from an image
```
$ docker container create/rm <imageName>
```
##### Start/stop a container
```
$ docker container start/stop <containerName>
```
##### Get info about the container
```
$ docker inspect <containerName>
```
##### Get into container bash
```
$ docker run -it -p port:port <imageName> 
$ docker container exec -it  <containerName> /bin/sh
```
##### Container Network
All containers inside a host have connectivity to each other 
```
$ docker network ls
$ docker network inspect
```

### Dockerfile 
##### Create an image and instantiate a container
```
# Pull the image from docker hub to create our custom image from that
FROM alpine
# Create a directory for code in the image and cd to that automatically
WORKDIR /app
# Copy files from our working directory in to the container
COPY . /app
# Run linux commands in image build step (also can be used for compiling)
RUN apk update && apk add nodejs
# Run commands after cotainer instantiation
CMD ["node","index.js"]
```
##### Build the image from Dockerfile in the current directory
```
$ docker build -t "<repoName>:<tagName>" .
```
### Docker compose
##### Create container from multiple images
```
version: '2.3'
services:

  backend:
    # build the image from Dockerfile
    build: ./backend
    working_dir: /app
    ports:
      - '9000:9000'
    environment:
      NODE_ENV: production
      APPID: 98d04d54d585e8499ac9d7da4ddd8900
    volumes:
    # Read the code from the local OS
     - ./backend:/app
     
  frontend:
    # Pull the image from registry
    image: registry.gitlab.com/<username>/<repository/><image>
    working_dir: /app
    ports:
      - '80:9000'
    environment:
      NODE_ENV: production
    volumes:
     - ./frontend:/app
```
### Docker volumes
##### Create/delete volume 
```
$ docker volume create --name name
$ docker volume ls
$ docker volume inspect name
$ docker volume rm
```
##### Create conatiner and mount the volume
```
$ docker run -it -v name:/mountpoint image
```
##### Usefull commands
```
$ docker-compose up
$ docker-compose start
$ docker-compose ps
$ docker-compose log -f

```
##### Push image to private docker registry
```
$ docker login
$ docker tag <imageName> <username>/<repository>
$ docker push <username>/<repository>
```
### [Refrence] (https://docs.docker.com)
